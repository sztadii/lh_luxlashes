var productList = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".productList");
    $slider = $el.find('.productList__slider');

    $el.imagesLoaded({background: true}).done(function () {
      $slider.slick({
        arrows: false,
        dots: false,
        dotsClass: 'slick__dots',
        customPaging: function () {
          return '<div class="slick__dot"></div>'
        },
        appendDots: $el,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        vertical: false,
        cssEase: 'linear',
        draggable: false,
        responsive: [
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    });
  };
};