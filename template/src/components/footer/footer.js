var footer = new function () {

  //private vars
  var timeout;

  //catch DOM
  var $fbPage = $('.fb-page');
  var $fbPageCopy = $fbPage.clone();

  //bind events
  $(window).resize(function () {
    clearTimeout(timeout);

    timeout = setTimeout(function () {
      $fbPage.html($fbPageCopy);
      FB.XFBML.parse();
    }, 1000);
  });

  //public functions
  this.loadApi = function () {
    var script = document.createElement("script");
    script.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8';
    document.body.appendChild(script);
  };
};

footer.loadApi();