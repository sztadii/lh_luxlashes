var banner = new function () {

  //private vars
  var animationTime;

  //catch DOM
  var $el;
  var $slider;
  var $sliderHeader;
  var $bannerDescItems;

  //bind events
  $(document).ready(function () {
    init();
    makeSlider();
    makeDesc();
    watchSlider();
    watchDesc();
  });

  //private functions
  var init = function () {
    $el = $('.banner.-main');
    $slider = $el.find('.banner__slider');
    $sliderHeader = $slider.find('.banner__header');
  };

  var makeSlider = function () {
    if ($slider.length > 0) {
      $slider.imagesLoaded({background: true}).done(function () {

        $slider.slick({
          infinite: true,
          dots: false,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: false,
          speed: 500,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });
      });
    }
  };

  var watchSlider = function () {

    $slider.on('init', function () {
      animateDesc(0);
    });

    $slider.on('beforeChange', function () {
      $sliderHeader.addClass('-hide');
    });

    $slider.on('afterChange', function () {
      $sliderHeader.removeClass('-hide');
      animateDesc($slider.slick('slickCurrentSlide'));
    });
  };

  var makeDesc = function () {
    var $sliderItems = $slider.find('.banner__item');
    var $bannerDesc = $('.banner__desc .container');


    $sliderItems.each(function (index) {
      var bannerDescInfo = '<div class="banner__descItem"><span class="banner__descItemIndex">'+ index +'</span><span class="banner__descTimer"></span><span class="banner__descItemText">'+ $(this).data('desc') + '</span></div>';
      $bannerDesc.append(bannerDescInfo);
    });
  };

  var watchDesc = function () {
    $bannerDescItems = $('.banner__descItem');

    $bannerDescItems.on('click', function () {
      $slider.slick('slickGoTo', $(this).index());
    });
  };

  var animateDesc = function (index) {
    var $actualDesc = $bannerDescItems.eq(index);
    var $actualDescTimer = $actualDesc.find('.banner__descTimer');
    var i = 0;

    $actualDescTimer.width(0);
    clearTimeout(animationTime);

    $bannerDescItems.removeClass('-active');
    $actualDesc.addClass('-active');

    animationTime = setInterval(function () {
      $actualDescTimer.width(i + '%');

      if(i++ == 100) {
        clearTimeout(animationTime);
        $slider.slick('slickNext');
      }
    }, 100);
  };
};
